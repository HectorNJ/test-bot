import telebot # Importamos las librería

from telebot import types

import json

# Ponemos nuestro Token generado con el @BotFather
TOKEN = '[TOKEN_TELEGRAM]' 

bot = telebot.AsyncTeleBot(TOKEN)

# cargamos archivo json con el cuestionario definida
archivo = open('cuestionario.json')

cuestionario = json.load(archivo)
archivo.close()

encuestas_activas = []

# Función para recuperar pregunta por id
def get_pregunta(id, lista): 
  res = None
  for obj in lista: 
      if obj['id'] == id: 
          res = obj
          break
  return res

#función que escucha y procesa los mensajes
def handle_messages(messages):
  for message in messages:
    encuesta_activa = 0 # iniciamos variable con estado de la encuesta

    # Validamos que sea el primer mensaje y agregamos nueva encuesta
    if message.text == "/encuesta":
      for encuesta in encuestas_activas:
        if encuesta["chat_id"] == message.chat.id:
          encuestas_activas.remove(encuesta)
          break
      encuesta_activa = 1
      # cramos nueva encuesta usando el chat_id
      encuestas_activas.append({
        "chat_id": message.chat.id,
        "username": message.chat.username,
        "first_name": message.chat.first_name,
        "last_name": message.chat.last_name,
        "cuestionario_indice": 1,
        "preguntas": []
      })

      
      pregunta = get_pregunta(1, cuestionario)
      bot.send_message(message.chat.id, pregunta["pregunta"])

    else:
      for encuesta in encuestas_activas:
        if encuesta["chat_id"] == message.chat.id:
          if encuesta["cuestionario_indice"] == 0:
            encuesta_activa = -1
            break

          encuesta_activa = 1

          pregunta = get_pregunta(encuesta["cuestionario_indice"], cuestionario)
          siguiente_pregunta = None
          if pregunta["tipo"] == "texto":
            encuesta["preguntas"].append({
              "id": pregunta["id"],
              "texto": pregunta["pregunta"],
              "respuesta": message.text
            })
            siguiente_pregunta = pregunta["siguiente_pregunta"]
          else:
            opcion_valida = False
            for opcion in pregunta["opciones"]:
              print(opcion)
              if opcion["valor"] == message.text:
                encuesta["preguntas"].append({
                  "id": pregunta["id"],
                  "texto": pregunta["pregunta"],
                  "respuesta": message.text
                })
                opcion_valida = True
                siguiente_pregunta = opcion["siguiente_pregunta"]
                break
            
            if opcion_valida == False:
              siguiente_pregunta = -1

          if siguiente_pregunta == None:
            encuesta_activa = False
            markup = types.ReplyKeyboardRemove(selective=False)
            bot.send_message(message.chat.id, 'Encuesta terminada. Gracias', reply_markup=markup)
            print("encuesta terminada... \n {}".format(encuesta))
            with open("encuesta_{}.txt".format(message.chat.id), 'w') as outfile:
              json.dump(encuesta, outfile)
          elif siguiente_pregunta == -1:
            pregunta = get_pregunta(encuesta["cuestionario_indice"], cuestionario)
            bot.send_message(message.chat.id, "Respuesta inválida...\n {}".format(pregunta["pregunta"]))
          else:
            encuesta["cuestionario_indice"] = siguiente_pregunta
            pregunta = get_pregunta(encuesta["cuestionario_indice"], cuestionario)
            if pregunta["tipo"] == "texto":
              markup = types.ReplyKeyboardRemove(selective=False)
              bot.send_message(message.chat.id, pregunta["pregunta"], reply_markup=markup)
            else:
              # agregamos opciones como botones
              markup = types.ReplyKeyboardMarkup()
              for opcion in pregunta["opciones"]:
                markup.add(opcion["valor"])
              bot.send_message(message.chat.id, pregunta["pregunta"], None , None, markup)
          break
    
    if encuesta_activa == 0:
      bot.send_message(message.chat.id, 'Para hacer una encuesta usa /encuesta')
    elif encuesta_activa == -1:
      markup = types.ReplyKeyboardRemove(selective=False)
      bot.send_message(message.chat.id, 'Ya respondiste la encuesta. Gracias', reply_markup=markup)
        
    # print("\n {}".format(encuestas_activas))

# agregamos un listener para escuchar los mensajes
bot.set_update_listener(handle_messages)
bot.polling()