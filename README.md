# Test bot

## Crear venv
```
py -m venv venv
```

## ejecutar
source venv/bin/activate

## Instalar dependencias
```
pip install -r requeriments.txt
```

## ejecutar
```
py telegram.py
```


## Modificar encuesta

La encuesta se debe crear en un archivo llamado cuestionario.json, se puede utilizar el ejemplo del archivo cuestionario.json.example
